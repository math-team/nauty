Source: nauty
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>,
           Doug Torrance <dtorrance@debian.org>
Build-Depends: debhelper-compat (= 13),
               help2man,
               libcliquer-dev,
               libgmp-dev,
               libtool,
               pkgconf,
               zlib1g-dev
Standards-Version: 4.6.2
Homepage: https://pallini.di.uniroma1.it
Vcs-Git: https://salsa.debian.org/math-team/nauty.git
Vcs-Browser: https://salsa.debian.org/math-team/nauty
Rules-Requires-Root: no

Package: libnauty-2.8.9
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: nauty (=${binary:Version}), nauty-doc
Multi-Arch: same
Description: library for graph automorphisms -- library package
 nauty (No AUTomorphisms, Yes?) is a set of procedures for computing
 automorphism groups of graphs and digraphs. This mathematical software
 suite is developed by Brendan McKay and Adolfo Piperno:
 http://pallini.di.uniroma1.it
 .
 nauty computes graph information in the form of a set of generators,
 the size of the group, and the orbits of the group; it can also
 produce a canonical label. The nauty suite is written in C and comes
 with a command-line interface, a collection of command-line tools,
 and an Application Programming Interface (API).
 .
 This package provides the shared libraries required to run programs
 compiled against the nauty library. To compile your own programs you
 also need to install the libnauty-dev package.

Package: nauty
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libnauty-2.8.9 (=${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Suggests: graphviz, nauty-doc
Description: library for graph automorphisms -- interface and tools
 nauty (No AUTomorphisms, Yes?) is a set of procedures for computing
 automorphism groups of graphs and digraphs. This mathematical software
 suite is developed by Brendan McKay and Adolfo Piperno:
 http://pallini.di.uniroma1.it
 .
 nauty computes graph information in the form of a set of generators,
 the size of the group, and the orbits of the group; it can also
 produce a canonical label. The nauty suite is written in C and comes
 with a command-line interface, a collection of command-line tools,
 and an Application Programming Interface (API).
 .
 This package provides the nauty interface named dreadnaut, and a
 small collection of utilities called gtools.

Package: libnauty-dev
Section: libdevel
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libnauty-2.8.9 (= ${binary:Version}), ${misc:Depends}
Conflicts: libnauty2-dev
Suggests: nauty-doc, pkgconf
Multi-Arch: same
Description: library for graph automorphisms -- development package
 nauty (No AUTomorphisms, Yes?) is a set of procedures for computing
 automorphism groups of graphs and digraphs. This mathematical software
 suite is developed by Brendan McKay and Adolfo Piperno:
 http://pallini.di.uniroma1.it
 .
 nauty computes graph information in the form of a set of generators,
 the size of the group, and the orbits of the group; it can also
 produce a canonical label. The nauty suite is written in C and comes
 with a command-line interface, a collection of command-line tools,
 and an Application Programming Interface (API).
 .
 This package contains the header files, static libraries and symbolic
 links that developers using the nauty API will need.

Package: nauty-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Enhances: libnauty-2.8.9 (=${binary:Version}), nauty (=${binary:Version}), libnauty-dev (=${binary:Version})
Suggests: pdf-viewer
Multi-Arch: foreign
Description: library for graph automorphisms -- user guide
 nauty (No AUTomorphisms, Yes?) is a set of procedures for computing
 automorphism groups of graphs and digraphs. This mathematical software
 suite is developed by Brendan McKay and Adolfo Piperno:
 http://pallini.di.uniroma1.it
 .
 This package provides the user guide for the nauty mathematical software
 suite; it also contains examples and extra technical documentations.
